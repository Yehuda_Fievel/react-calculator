import React from 'react';

const Button = ({ className, value, onClick }) => {
  return (
    <button onClick={onClick} className={`btn ${className}`}>
      {value}
    </button>
  );
};

export default Button;
