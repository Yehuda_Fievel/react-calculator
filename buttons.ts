const btnNames = [
    {
      className: 'other',
      value: 'AC',
    },
    {
      className: 'other',
      value: '+-',
    },
    {
      className: 'other',
      value: '%',
    },
    {
      className: 'actions',
      value: '/',
    },
    {
      className: 'numbers',
      value: 7,
    },
    {
      className: 'numbers',
      value: 8,
    },
    {
      className: 'numbers',
      value: 9,
    },
    {
      className: 'actions',
      value: '*',
    },
    {
      className: 'numbers',
      value: 4,
    },
    {
      className: 'numbers',
      value: 5,
    },
    {
      className: 'numbers',
      value: 6,
    },
    {
      className: 'actions',
      value: '-',
    },
    {
      className: 'numbers',
      value: 1,
    },
    {
      className: 'numbers',
      value: 2,
    },
    {
      className: 'numbers',
      value: 3,
    },
    {
      className: 'actions',
      value: '+',
    },
    {
      className: 'numbers',
      value: 0,
    },
    {
      className: 'numbers',
      value: '.',
    },
    {
      className: 'actions equals',
      value: '=',
    },
  ];
  
  export default btnNames;
  