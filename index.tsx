import React, { Component } from 'react';
import { render } from 'react-dom';
import Button from './Button';
import btnNames from './buttons';
import './style.css';

// https://help.apple.com/assets/61606EE5D7F26F422E7EB450/61606EEAD7F26F422E7EB468/en_US/444e9701b92783985608b59943f635be.png
// design a calcualtor that looks like this and behaves as a normal calcualtor. You can ignore the %, +/- and decimal button functionaltiy. still render it, but when you click it, have it do nothing

interface AppProps {}

interface AppState {
  currentValue: string;
  savedValue: string;
  currentAction: string;
}

class App extends Component<AppProps, AppState> {
  actionList: any[] = [];
  constructor(props) {
    super(props);
    this.state = {
      currentValue: '0',
      savedValue: '',
      currentAction: '',
    };
  }

  onClick = (btn) => {
    if (isNaN(btn.value)) {
      switch (btn.value) {
        case '%':
        case '+-':
          return;
        case 'AC':
          this.setState({
            currentAction: '',
            savedValue: '',
            currentValue: '0',
          });
          break;
        case '/':
        case '*':
        case '-':
        case '+':
          this.setState({
            currentAction: btn.value,
            savedValue: this.state.currentValue,
          });
          break;
        case '.':
          if (!this.state.currentValue.includes('.')) {
            this.setState({
              currentValue: this.state.currentValue + btn.value,
            });
          }
          break;
        case '=':
          this.handleEqual();
          break;
        default:
          break;
      }
    } else {
      if (this.state.currentAction) {
        this.setState({
          savedValue: this.state.currentValue,
          currentValue: btn.value.toString(),
        });
      } else {
        const currentValue = parseInt(this.state.currentValue)
          ? this.state.currentValue + btn.value
          : btn.value;
        this.setState({
          currentValue: currentValue.toString(),
        });
      }
    }
  };

  handleEqual = () => {
    const savedValue = parseFloat(this.state.savedValue);
    const currentValue = parseFloat(this.state.currentValue);
    let newValue;
    switch (this.state.currentAction) {
      case '/':
        newValue = savedValue / currentValue;
        break;
      case '*':
        newValue = savedValue * currentValue;
        break;
      case '-':
        newValue = savedValue - currentValue;
        break;
      case '+':
        newValue = savedValue + currentValue;
        break;
      default:
        return;
    }
    this.setState({ currentValue: newValue.toString(), currentAction: '' });
  };

  render() {
    return (
      <div className="background">
        <div className="screen">{this.state.currentValue}</div>
        <div className="btn-container">
          {btnNames.map((btn) => (
            <Button
              className={btn.className}
              value={btn.value}
              onClick={() => this.onClick(btn)}
            />
          ))}
        </div>
      </div>
    );
  }
}

render(<App />, document.getElementById('root'));
